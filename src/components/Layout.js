import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import { connect } from 'react-redux';

import Aux from '../hoc/Wrapper';

class Layout extends Component {

    render() {
        console.log(this.props.loggedIn, localStorage.getItem("loggedIn"));
        return (
            <Aux>
                <nav className="navbar navbar-light bg-light">
                    <Link to="/" className="navbar-brand" href="/">Navbar</Link>
                    <ul className="navbar-nav ml-auto flex-row">
                        {this.props.loggedIn || localStorage.getItem("loggedIn")
                        ?
                            (
                                <li className="nav-item px-2">
                                    <Link to="/logout" className="nav-link" href="/logout">Logout</Link>
                                </li>
                            )
                        :
                            (
                                <Aux>
                                    <li className="nav-item px-2">
                                        <Link to="/login" className="nav-link" href="/login">Login</Link>
                                    </li>
                                    <li className="nav-item px-2">
                                        <Link to="/register" className="nav-link" href="/register">Register</Link>
                                    </li>
                                </Aux>
                            )
                        }
                    </ul>
                </nav>
                {this.props.children}
            </Aux>
        );
    }
};

const mapStateToprops = state => {
    return {
        loggedIn: state.login.loggedIn,
        error: state.login.error
    }
}

export default connect(mapStateToprops)(Layout);