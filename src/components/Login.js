import React, { Component } from 'react';

import { connect } from 'react-redux';

import { Link, Redirect } from 'react-router-dom';

import * as actions from '../store/actions/login';

class Login extends Component {
    state = {
        loginCredentials: {
            email: '',
            password: ''
        },
        error: false,
        loggedIn: false
    };

    inputChangeHandler = (event) => {
        const currentState = Object.assign({}, this.state.loginCredentials);

        currentState[event.target.id] = event.target.value;
        this.setState( { loginCredentials: { ...currentState } } );
        console.log(this.state);
    }

    onSubmitHandler = (event) => {
        event.preventDefault();
        const user = Object.assign({}, this.state.loginCredentials);
        console.log("Email: " + user.email);
        console.log("Password: " + user.password);
        this.props.onLogin(user.email, user.password);
    }

    render() {
        return (
            this.props.loggedIn || localStorage.getItem("loggedIn")
            ? <Redirect to="/" />
            :
            <div className="shadow rounded w-75 mx-auto bg-white rounded">
                <h3 className="text-center mb-3 bg-light p-3 rounded-top">
                    Login
                </h3>
                <form 
                    className="d-flex flex-column p-3 rounded-bottom"
                    onSubmit={this.onSubmitHandler}>
                    <div className="from-group">
                        <label htmlFor="email">Email</label>
                        <input 
                            type="email" 
                            className="form-control" 
                            id="email" 
                            placeholder="Enter Email"
                            onChange={(event) => this.inputChangeHandler(event)}/>
                    </div>
                    <div className="from-group">
                        <label htmlFor="password">Password</label>
                        <input 
                            type="password" 
                            className="form-control" 
                            id="password" 
                            placeholder="Enter Password"
                            onChange={(event) => this.inputChangeHandler(event)}/>
                    </div>
                    <button className="btn btn-primary align-self-end mt-3">Login</button>
                    <p>Don't have an account? <Link to="/register" className="nav-link p-0 pl-1 d-inline">Register Now!</Link></p>
                </form>
            </div>
        );
    }
};

const mapStateToProps = state => {
    return {
        loggedIn: state.login.loggedIn,
        error: state.login.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogin: (email, password) => dispatch(actions.login(email, password))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);