import axios from 'axios';

import * as actionTypes from './actionTypes';

export const loginStart = () => {
    return {
        type: actionTypes.LOGIN_START
    };
};

export const loginSuccess = (loginData) => {
    return {
        type: actionTypes.LOGIN_SUCCESS,
        loginData: loginData
    };
};

export const loginFail = (error) => {
    return {
        type: actionTypes.LOGIN_FAIL,
        error: error
    };
};

export const login = (email, password) => {
    return dispatch => {
        dispatch(loginStart());
        let params = {};
        if(email && password) {
            params.email = email;
            params.username = password;
        }
        axios.get('https://jsonplaceholder.typicode.com/users', {params})
        .then(
            response => {
                console.log(response.data);
                if(response.data.length > 0) {
                    console.log("Logged In");
                    dispatch(loginSuccess());
                    localStorage.setItem("loggedIn", true);
                }
                else {
                    console.log("Incorrect Credentials");
                    dispatch(loginFail());
                }
            }
        )
        .catch(
            error => {
                console.log(error);
            }
        );
    };
};

export const logoutProcess = () => {
    return {
        type: actionTypes.LOGOUT
    };
};

export const logout = () => {
    return dispatch => {
        localStorage.removeItem("loggedIn");
        dispatch(logoutProcess());
    }
}