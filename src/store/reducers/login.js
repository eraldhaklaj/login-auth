import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loggedIn: false,
    error: false
}

const loginSuccess = ( state ) => {
    return {
        ...state,
        loggedIn: true,
        error: false
    };
};

const loginFail = ( state ) => {
    return {
        ...state,
        loggedIn: false,
        error: true
    }
};

const logout = ( state ) => {
    return {
        ...state,
        loggedIn: false,
        error: false
    }
};

const reducer = (state = initialState, action) => {
    switch ( action.type ) {
        case actionTypes.LOGIN_SUCCESS: return loginSuccess(state);
        case actionTypes.LOGIN_FAIL: return loginFail(state);
        case actionTypes.LOGOUT: return logout(state);
        default: return state
    }
};

export default reducer;