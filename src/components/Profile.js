import React, {Component} from 'react';

import { Redirect } from 'react-router-dom';

import { connect } from 'react-redux';

class Profile extends Component {

    render() {
        return (
            !this.props.loggedIn && !localStorage.getItem("loggedIn")
            ? <Redirect to="/login" />
            :
            <h1 className="text-center">Profile</h1>
        );
    };
};

const mapStateToProps = state => {
    return {
        loggedIn: state.login.loggedIn,
        error: state.login.error
    }
}

export default connect(mapStateToProps)(Profile);