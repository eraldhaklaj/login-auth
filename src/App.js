import React, {Component} from 'react';

import { Route, Switch } from 'react-router-dom';

import Layout from './components/Layout';
import Login from './components/Login';
import Logout from './components/Logout';
import Register from './components/Register';
import Profile from './components/Profile';

class App extends Component {
  render() {
    return (
      <Layout>
        <div className="container py-4">
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
            <Route path="/logout" component={Logout} />
            <Route path="/" component={Profile} />
          </Switch>
        </div>
      </Layout>
    );
  };
};

export default App;
