import React, {Component} from 'react';

import { connect } from 'react-redux';

import { Link, Redirect } from 'react-router-dom';

class Register extends Component {

    render() {
        return (
            this.props.loggedIn || localStorage.getItem("loggedIn")
            ? <Redirect to="/" />
            :
            <div className="p-3 shadow radius w-75 mx-auto bg-white">
                <h3 className="text-center mb-3">
                    Register
                </h3>
                <form className="d-flex flex-column">
                    <div className="from-group">
                        <label htmlFor="email">Email</label>
                        <input type="email" className="form-control" id="email" placeholder="Enter Email"/>
                    </div>
                    <div className="from-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" id="password" placeholder="Enter Password"/>
                    </div>
                    <button className="btn btn-primary align-self-end mt-3">Register</button>
                    <p>Have an account? <Link to="/login" className="nav-link p-0 pl-1 d-inline">Login here!</Link></p>

                </form>
            </div>
        );
    }
};

const mapStateToProps = state => {
    return {
        loggedIn: state.login.loggedIn,
        error: state.login.error
    }
}

export default connect(mapStateToProps)(Register);